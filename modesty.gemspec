$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "modesty/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "modesty"
  s.version     = Modesty::VERSION
  s.authors     = ["eugen mueller"]
  s.email       = ["egnmueller@googlemail.com"]
  s.homepage    = "TODO"
  s.summary     = "TODO: Summary of Modesty."
  s.description = "TODO: Description of Modesty."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.4"

  s.add_development_dependency "pg"
end
