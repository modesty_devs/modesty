module Modesty
  class Engine < ::Rails::Engine
    isolate_namespace Modesty
  end
end
